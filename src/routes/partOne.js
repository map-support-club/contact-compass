export default [
  {
    name: 'adultChildSex',
    question: 'Sexual contact between children and adults is harmful.',
    response: 0,
    invert: false,
    part: 0,
  },

  {
    name: 'childConsent',
    question: 'Children cannot consent.',
    response: 0,
    invert: false,
    part: 0,
  },

  {
    name: 'healthyChildrenBehaveSexually',
    question: 'Healthy children do not behave sexually.',
    response: 0,
    invert: false,
    part: 0,
  },

  {
    name: 'childAdultSexLegal',
    question:
      'If it was legal, children and adults could have sex without it being harmful.',
    response: 0,
    invert: true,
    part: 0,
  },

  {
    name: 'childIntelligent',
    question:
      'Children are a lot more intelligent than most people give them credit for.',
    response: 0,
    invert: true,
    part: 0,
  },

  {
    name: 'childChildConsent',
    question:
      'Children can consent to sex with other children but not with adults.',
    response: 0,
    invert: false,
    part: 0,
  },

  {
    name: 'childrenUndersandConsent',
    question:
      'Children would be able to understand consent if it was taught to them from a young age.',
    response: 0,
    invert: true,
    part: 0,
  },
]
