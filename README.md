# Contact Compass 

The Contact Compass is a tool intended to assess individuals' views on sexual contact between children and adults.

Developed with 🔬 by OliverViking and Miami Autumn
